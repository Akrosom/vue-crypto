const API_KEY =
  "9ea1362f70325c3be4362c2ce171d35af069b93618ec96774c71ae547dfb9cec";

export const getAllCurrencies = () => {
  return fetch(
    `https://min-api.cryptocompare.com/data/blockchain/list?api_key=${API_KEY}`
  ).then((data) => data.json());
};

const socket = new WebSocket(
  `wss://streamer.cryptocompare.com/v2?api_key=${API_KEY}`
);

export const getTicker = (setPrice) => {
  socket.addEventListener("message", setPrice);
};

export const subscribeToTicker = (ticker) => {
  const message = JSON.stringify({
    action: "SubAdd",
    subs: [`5~CCCAGG~${ticker}~USD`],
  });
  if (socket.readyState === WebSocket.OPEN) {
    socket.send(message);
    return;
  }
  socket.addEventListener(
    "open",
    () => {
      socket.send(message);
    },
    { once: true }
  );
};

export const unsubscribeFromTicker = (ticker) => {
  const message = JSON.stringify({
    action: "SubRemove",
    subs: [`5~CCCAGG~${ticker}~USD`],
  });
  socket.send(message);
};

export const closeSocket = () => {
  socket.close();
};
